"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
const React = qu4rtet.require("react");
const Component = React.Component;

var _qu4rtet$require = qu4rtet.require("react-intl");

const FormattedMessage = _qu4rtet$require.FormattedMessage;

var _qu4rtet$require2 = qu4rtet.require("./components/layouts/Panels");

const RightPanel = _qu4rtet$require2.RightPanel;

var _qu4rtet$require3 = qu4rtet.require("@blueprintjs/core");

const Card = _qu4rtet$require3.Card,
      Callout = _qu4rtet$require3.Callout,
      Tree = _qu4rtet$require3.Tree,
      ITreeNode = _qu4rtet$require3.ITreeNode;


class APITree extends Component {
  constructor(props) {
    super(props);

    this.handleNodeClick = (nodeData, _nodePath, evt) => {
      const originallySelected = nodeData.isSelected;
      nodeData.isSelected = originallySelected == null ? true : !originallySelected;
      this.setState(this.state);
    };

    this.handleNodeCollapse = nodeData => {
      nodeData.isSelected = false;
      this.setState(this.state);
    };

    this.handleNodeExpand = nodeData => {
      nodeData.isExpanded = true;
      this.setState(this.state);
    };

    this.state = { nodes: [] };
  }
  componentDidMount() {
    let nodes = [{ id: 1, icon: "folder-close", label: "Item 1" }, {
      id: 2,
      icon: "folder-close",
      label: "Item 2",
      childNodes: [{ id: 3, icon: "document", label: "Nested Item 1" }]
    }];
    this.setState({ nodes: nodes });
  }
  render() {
    return React.createElement(Tree, { contents: this.state.nodes, onNodeClick: this.handleNodeClick });
  }
}

class ExampleScreen extends Component {
  render() {
    return React.createElement(
      RightPanel,
      {
        title: React.createElement(FormattedMessage, { id: "plugins.boilerplate.developerInfo" }) },
      React.createElement(
        "div",
        { className: "large-cards-container" },
        React.createElement(
          Card,
          null,
          React.createElement(
            "h5",
            null,
            React.createElement(FormattedMessage, { id: "plugins.boilerplate.developerInfo" })
          ),
          React.createElement(
            Callout,
            null,
            "This is an example of a screen."
          ),
          React.createElement(APITree, null)
        )
      )
    );
  }
}
exports.ExampleScreen = ExampleScreen;